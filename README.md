# `@thorchain/asgardex-theme`

Global theme for asgardex UI

## Installation

```
yarn add @thorchain/asgardex-theme
```

## Usage

### Theme config

```
import themes from '@thorchain/asgardex-theme'

themes.dark // dark theme
themes.light // light theme
```

### Theme type

```
import { ThemeType } from '@thorchain/asgardex-theme'

// dark
ThemeType.DARK
// light
ThemeType.LIGHT
```
