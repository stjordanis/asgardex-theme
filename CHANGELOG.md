# v.0.1.1 (2020-05-23)

- Update to latest dependencies
- Extract library from mono-repo of `asgardex-common` to its own repository https://gitlab.com/thorchain/asgardex-common/asgardex-theme

# v.0.1.0 (2020-05-12)

First release
